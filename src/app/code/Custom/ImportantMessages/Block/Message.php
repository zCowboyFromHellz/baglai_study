<?php


namespace Custom\ImportantMessages\Block;


use Magento\Framework\View\Element\Template;

class Message extends \Magento\Framework\View\Element\Template
{


    /**
     * @var \Custom\ImportantMessages\Helper\Data
     */
    private $helperData;
    public $getEnabled;
    public $getText;

    public function __construct(Template\Context $context, \Custom\ImportantMessages\Helper\Data $helperData, array $data = [])
    {
        parent::__construct($context, $data);
        $this->helperData = $helperData;
    }

    public function getEnabledMessage()
    {
        return (int)$this->helperData->getGeneralConfig('enable');

    }

    public function getTextMessage()
    {
        return nl2br( $this->helperData->getGeneralConfig('display_text'));
    }

}

