<?php


namespace Baglai\HelloWorld\Controller\Index;


class Test extends \Magento\Framework\App\Action\Action
{

    public function execute()
    {
        $textDisplay = new \Magento\Framework\DataObject(array('text' => 'Baglai'));
        $this->_eventManager->dispatch('baglai_helloworld_display_text', ['mp_text' => $textDisplay]);
        echo $textDisplay->getText();
        exit;
    }
}