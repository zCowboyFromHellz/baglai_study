<?php


namespace Baglai\HelloWorld\Model;


class Post extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Baglai\HelloWorld\Model\ResourceModel\Post::class);
    }
}