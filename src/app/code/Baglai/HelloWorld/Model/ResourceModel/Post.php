<?php


namespace Baglai\HelloWorld\Model\ResourceModel;


class Post extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init(\Baglai\HelloWorld\Api\Data\PostDataManagement::DB_TABLE, 'post_id');
    }

}