<?php


namespace Baglai\HelloWorld\Model\ResourceModel;


class Topic extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Baglai\HelloWorld\Model\Topic', 'Baglai\HelloWorld\Model\ResourceModel\Topic');
    }
}