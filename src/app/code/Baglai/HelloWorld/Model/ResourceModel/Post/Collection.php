<?php


namespace Baglai\HelloWorld\Model\ResourceModel\Post;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'post_id';
    protected $_eventPrefix = 'baglai_helloworld_post_collection';
    protected $_eventObject = 'post_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Baglai\HelloWorld\Model\Post::class, \Baglai\HelloWorld\Model\ResourceModel\Post::class);
    }

}