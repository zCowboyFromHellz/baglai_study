<?php


namespace Baglai\HelloWorld\Model;


class Topic extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('baglai', 'topic_id');
    }
}