<?php


namespace Baglai\HelloWorld\Plugin;


class ExamplePlugin
{
    public function beforeSetTitle(\Baglai\HelloWorld\Controller\Index\Example $subject, $title)
    {
        $title = $title . " to ";
        echo __METHOD__ . "</br>";

        return [$title];
    }


    public function afterGetTitle(\Baglai\HelloWorld\Controller\Index\Example $subject, $result)
    {

        echo __METHOD__ . "</br>";

        return '<h1>'. $result . 'UBERVAFLIA' .'</h1>';

    }


    public function aroundGetTitle(\Baglai\HelloWorld\Controller\Index\Example $subject, callable $proceed)
    {

        echo __METHOD__ . " - Before proceed() </br>";
        $result = $proceed();
        echo __METHOD__ . " - After proceed() </br>";


        return $result;
    }
}