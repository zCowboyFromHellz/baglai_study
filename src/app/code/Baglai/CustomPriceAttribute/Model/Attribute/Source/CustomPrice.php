<?php


namespace Baglai\CustomPriceAttribute\Model\Attribute\Source;


class CustomPrice extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('Top left'),     'value' => '1'],
                ['label' => __('Top right'),    'value' => '2'],
                ['label' => __('Bottom left'),  'value' => '3'],
                ['label' => __('Bottom right'), 'value' => '4'],
            ];
        }
        return $this->_options;
    }
}