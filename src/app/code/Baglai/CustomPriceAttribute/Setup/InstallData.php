<?php


namespace Baglai\CustomPriceAttribute\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'custom_price',
            [
                'group' => 'Price option',
                'type' => 'decimal',
                'backend' => 'Baglai\CustomPriceAttribute\Model\Attribute\Backend\CustomPrice',
                'frontend' => 'Baglai\CustomPriceAttribute\Model\Attribute\Frontend\CustomPrice',
                'label' => 'Custom price',
                'input' => 'price',
                'class' => '',
                'source' => \Baglai\CustomPriceAttribute\Model\Attribute\Source\CustomPrice::class,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'input_renderer' => '',
                'visible_on_front' => true,
                'is_used_in_grid' => true,
                'is_visible_in_grid' => false,
                'is_filterable_in_grid' => false,
            ]
        );
    }
}