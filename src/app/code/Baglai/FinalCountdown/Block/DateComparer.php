<?php


namespace Baglai\FinalCountdown\Block;

use Magento\Framework\Stdlib\DateTime\DateTime;

class DateComparer extends \Magento\Catalog\Block\Product\View
{

    /**
     * @var \Magento\CatalogRule\Model\ResourceModel\RuleFactory
     */
    private $ruleFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTimeFactory
     */
    private $dateTimeFactory;
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $_customerSession;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * DateTime in UNIX
     * @var
     */
    private $fastestDate;
    /**
     * @var bool
     */
    private $firstDateOccur = true;
    /**
     * Output format of time
     * @var
     */
    private $formatOutput = 'Y/m/d';

    /**
     * SpecialPriceCountDown constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Url\EncoderInterface $urlEncoder
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\CatalogRule\Model\ResourceModel\RuleFactory $rules
     * @param \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateTimeFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\CatalogRule\Model\ResourceModel\RuleFactory $rules,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateTimeFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->dateTimeFactory = $dateTimeFactory;
        $this->ruleFactory = $rules;
        $this->_customerSession = $customerSession;
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data);
    }

    /**
     * Entry point
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */

    public function getProductSale()
    {
        $_product = $this->getProduct();
        $end_date_special = $this->dateTimeFactory->create();
        $endSpecial = $end_date_special->date(null, $_product->getData('special_to_date'));

        $rules = $this->getCatalogPriceRuleFromProduct($_product, $this->getGroupId());
        $rulesDate = '';

        foreach ($rules as $rule) {

            $rulesDate = $this->dateResolverTimes($rule['to_time']);
        }
        $rulesDate = $this->dateTimeFactory->create()->date(null, $rulesDate);
        $fastest_date = $this->dateResolver($endSpecial, $rulesDate);
        //fastest date
        $product = $fastest_date;
        // if date is behind
        $date = $this->dateTimeFactory->create();
        $dateTs = $date->gmtDate();
        if ($dateTs >= $product) {
            $formattedProductDate = true;
        } else {
            $formattedProductDate = $this->dateTimeFactory->create()->date($this->getFormatOutput(), $product);
        }
        return $formattedProductDate;
    }

    public function getProductData()
    {
        return $this->getProduct();
    }

    public function specialToDate()
    {
        $this->getProduct();
        $this->date = $this->dateTimeFactory->create();
        $endSpecialDate = $this->dateTimeFactory->create();
        $productData = $this->getProductData();
        return $endSpecialDate->date('Y-m-d H:i:s', $productData->getSpecialToDate());

    }

    /** Get Catalog Price Rule by Product
     * @param $product
     * @param $customerGroupId
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getCatalogPriceRuleFromProduct($product, $customerGroupId)
    {
        $storeId = $product->getStoreId();
        $store = $this->storeManager->getStore($storeId);
        $websiteId = $store->getWebsiteId();

        $date = $this->dateTimeFactory->create();
        $dateTs = $date->gmtDate();
        /**
         * @var \Magento\CatalogRule\Model\ResourceModel\RuleFactory
         */
        $resource = $this->ruleFactory->create();

        $rules = $resource->
        getRulesFromProduct($dateTs, $websiteId, $customerGroupId, $product->getIdBySku($product['sku']));

        return $rules;
    }

    /**
     * Get group id from session
     */
    private function getGroupId()
    {
        if ($this->_customerSession->isLoggedIn()) {
            return $customerGroup = $this->_customerSession->getCustomer()->getGroupId();
        } else {
            return 0;
        }
    }

    /**
     * Resolve array of dates which that comes faster
     * @param $date
     * @param bool $first
     * @return mixed
     */
    private function dateResolverTimes($date, $first = true)
    {
        if ($this->firstDateOccur || $first) {
            unset($this->fastestDate);
            $this->fastestDate = $date;
            $this->firstDateOccur = false;
            return $this->fastestDate;
        } else {
            $fastestDate = ($date < $this->fastestDate) ? $date : $this->fastestDate;
        }

        return $fastestDate;
    }

    /**
     * Resolve only two date which that comes faster
     * @param $special
     * @param $catalogRule
     * @return mixed
     */
    private function dateResolver($special, $catalogRule)
    {
        $fastestDate = ($special < $catalogRule) ? $special : $catalogRule;
        return $fastestDate;
    }

    /**
     * Get TIme format
     * @return mixed
     */
    public function getFormatOutput()
    {
        return $this->formatOutput;
    }

    /**
     * Set time format
     * @param $format
     */
    public function setFormatOutput($format)
    {
        $this->formatOutput = $format;
    }
}
