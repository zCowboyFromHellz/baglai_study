<?php


namespace Baglai\FinalCountdown\Block;


use Magento\Catalog\Api\ProductRepositoryInterface;

class Custom extends \Magento\Catalog\Block\Product\View
{
    /**
     * @var \Magento\Catalog\Block\Product\Context
     */
    private $context;
    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    private $jsonEncoder;
    /**
     * @var \Magento\Catalog\Helper\Product
     */
    private $productHelper;
    /**
     * @var \Magento\Framework\Locale\FormatInterface
     */
    private $localeFormat;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;
    /**
     * @var array
     */
    private $data;
    /**
     * @var \DateTimeFactory
     */
    private $dateTimeFactory;
    /**
     * @var \DateTimeFactory
     */
    private $dateTimeFactoryPHP;
    private $storeManager;

    /**
     * Custom constructor.
     * @param \DateTimeFactory $dateTimeFactoryPHP
     * @param \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateTimeFactory
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Url\EncoderInterface $urlEncoder
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Customer\Model\Session $customerSession
     * @param ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
        \DateTimeFactory $dateTimeFactoryPHP,
        \Magento\Framework\Stdlib\DateTime\DateTimeFactory $dateTimeFactory,
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\CatalogRule\Model\ResourceModel\RuleFactory $rules,
        array $data = [])
    {
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data);

        $this->context = $context;
        $this->urlEncoder = $urlEncoder;
        $this->jsonEncoder = $jsonEncoder;
        $this->string = $string;
        $this->productHelper = $productHelper;
        $this->productTypeConfig = $productTypeConfig;
        $this->localeFormat = $localeFormat;
        $this->customerSession = $customerSession;
        $this->productRepository = $productRepository;
        $this->priceCurrency = $priceCurrency;

        $this->dateTimeFactory = $dateTimeFactory;
        $this->dateTimeFactoryPHP = $dateTimeFactoryPHP;
        $this->storeManager = $storeManager;
        $this->ruleFactory = $rules;

    }

    public function getProductData()
    {
        return $this->getProduct();
    }

    public function execute()
    {
        $this->date = $this->dateTimeFactory->create();
        $productData = $this->getProductData();
        if ($productData->getSpecialPrice() && ($productData->getSpecialToDate() || $productData->getSpecialFromDate())) {           //Проверка товара на наличие спец. цены и даты начала и окончания
            $currentDate = $this->dateTimeFactory->create();                                                                         //Получение текущей даты
            $endSpecialDate = $this->dateTimeFactory->create();                                                                      //
            $currentDate->date(null, $this->date->date());
            $isSpecialDateToPresented = false;
            if ($productData->getSpecialToDate()) {
                $isSpecialDateToPresented = true;
            }
            if ($isSpecialDateToPresented) {
                $endSpecialDate->date('Y-m-d H:i:s', $productData->getSpecialToDate());                                        //Приведение вида даты окончания спец. цены к шаблону
                $comparedDate = $this->dateComparer($currentDate->timestamp(), $endSpecialDate->timestamp());                        //
                $timeStamp = $this->dateTimeFactoryPHP->create()->setTimestamp($comparedDate ? $comparedDate : null);
                $result = $timeStamp->format('Y-m-d H:i:s');
                return $result;
            }
        }
    }

    public function dateComparer($currentDate, $endSpecialDate)
    {
        if ($currentDate <= $endSpecialDate) {
            return $endSpecialDate;
        } else {
            return false;
        }
    }

    public function getSpecDateTo()
    {
        $productData = $this->getProductData();
        if ($productData->getSpecialToDate()) {
            return true;
        }
    }

    private function getCatalogPriceRuleFromProduct($product, $customerGroupId)
    {
        $storeId = $product->getStoreId();
        $store = $this->storeManager->getStore($storeId);
        $websiteId = $store->getWebsiteId();

        $date = $this->dateTimeFactory->create();
        $dateTs = $date->gmtDate();
        /**
         * @var \Magento\CatalogRule\Model\ResourceModel\RuleFactory
         */
        $resource = $this->ruleFactory->create();

        $rules = $resource->getRulesFromProduct($dateTs, $websiteId, $customerGroupId, $product->getIdBySku($product['sku']));

        return $rules;
    }
}

