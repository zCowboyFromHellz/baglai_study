<?php


namespace Baglai\ScreenPosition\Helper;


use Magento\Framework\App\Helper\Context;

class Custom extends \Magento\Framework\App\Helper\AbstractHelper
{
    /** @var array for validate s */

    private $_availableSku = [
        'WS01',
        'WS05',
        'WS10',
        'WS02',
        'WS01',
        'WS12',
        'WS11',
        'WS03',
        'WS06',
        'WS05',
        'WS09',
        'WS04',
        'MT01',
        'MT05',
        'MT10',
        'MT02',
        'MT01',
        'MT12',
        'MT11',
        'MT03',
        'MT06',
        'MT05',
        'MT09',
        'MT04'
        ];

    /**
     * @param string $sku
     * @return bool
     */
    public function validateProductBySku($sku)
    {
        if(in_array($sku, $this->getValidSkuArray())) {
            return true;
        }   else {
            return false;
        }
    }

    /** @return array */

    protected function getValidSkuArray()
    {
        return $this->_availableSku;
    }

}