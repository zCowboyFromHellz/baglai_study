<?php


namespace Baglai\ScreenPosition\Model\Attribute\Source;


class BodyPosition extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /*
     * make a positions
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('Top left'),     'value' => 'top_left'],
                ['label' => __('Top right'),    'value' => 'top_right'],
                ['label' => __('Bottom left'),  'value' => 'bottom_left'],
                ['label' => __('Bottom right'), 'value' => 'bottom_right'],
            ];
        }
        return $this->_options;
    }
}