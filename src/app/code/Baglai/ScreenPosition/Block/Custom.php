<?php


namespace Baglai\ScreenPosition\Block;


use Magento\Catalog\Api\ProductRepositoryInterface;

class Custom extends \Magento\Catalog\Block\Product\View
{
    /**
     * Vivod FinalPrice v block customValue
     * @var \Magento\Catalog\Model\Product
     */
    protected $productModel;
    protected $_customHelper;

    public function getCustomData()
    {
        $customProduct = $this->getProduct();
        $customValue = "Value : ";
        return $customValue . $customProduct->getFinalPrice();
    }
    public function __construct(\Magento\Catalog\Block\Product\Context $context,
                                \Magento\Framework\Url\EncoderInterface $urlEncoder,
                                \Magento\Framework\Json\EncoderInterface $jsonEncoder,
                                \Magento\Framework\Stdlib\StringUtils $string,
                                \Magento\Catalog\Helper\Product $productHelper,
                                \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
                                \Magento\Framework\Locale\FormatInterface $localeFormat,
                                \Magento\Customer\Model\Session $customerSession,
                                ProductRepositoryInterface $productRepository,
                                \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
                                \Baglai\ScreenPosition\Helper\Custom $customHelper,
                                array $data = [])
    {
        $this->_customHelper = $customHelper;
        parent::__construct($context,
                            $urlEncoder,
                            $jsonEncoder,
                            $string,
                            $productHelper,
                            $productTypeConfig,
                            $localeFormat,
                            $customerSession,
                            $productRepository,
                            $priceCurrency,
                            $data);
    }

    /** return bool */
    public function isAvailable()
    {
        $currentProduct = $this->getProduct();
        return $this->_customHelper->validateProductBySku($currentProduct->getSku());
    }
}