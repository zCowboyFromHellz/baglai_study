<?php


namespace Baglai\BodyPosition\Model\Config\Source;


class Custom implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     * list of settings of module
     */
    public function toOptionArray()
    {

        return [
            ['value' => 0, 'label' => __('Disabled')],
            ['value' => 1, 'label' => __('Enabled')],
        ];
    }
}