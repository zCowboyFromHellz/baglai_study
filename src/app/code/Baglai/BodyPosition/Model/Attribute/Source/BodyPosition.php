<?php


namespace Baglai\BodyPosition\Model\Attribute\Source;


class BodyPosition extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /*
     * make a positions
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('Top left'),     'value' => '1'],
                ['label' => __('Top right'),    'value' => '2'],
                ['label' => __('Bottom left'),  'value' => '3'],
                ['label' => __('Bottom right'), 'value' => '4'],
            ];
        }
        return $this->_options;
    }
}