<?php


namespace Baglai\BodyPosition\Block;


use Magento\Catalog\Api\ProductRepositoryInterface;

class Custom extends \Magento\Catalog\Block\Product\View
{
    protected $config;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Baglai\BodyPosition\Helper\Config $config,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data
        );
        $this->config = $config;
    }


    public function getBodyPosition()
    {
        $product = $this->getProduct();
        $bodyPosition = $product->getData('body_position');
        return $bodyPosition;
    }

    /**
     * @return bool
     */
    public function checkEnableBodyPosition()
    {
        return $this->config->isEnabledBaglaiPashcoin() === '1';
    }

}