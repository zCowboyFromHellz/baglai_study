<?php


namespace Baglai\BodyPosition\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;


class Config extends AbstractHelper
{
    protected $scopeConfig;

    /**
     * @var ScopeConfigInterface
     */
   protected $blabla;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig

    )
    {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }


//    const XML_PATH_MYMODULE = 'balai_pashcoin';

    public function isEnabledBaglaiPashcoin()
    {
        return $this->scopeConfig->getValue('setmod/set_list/dropdown_test',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

    }

}