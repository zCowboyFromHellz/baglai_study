<?php


namespace Baglai\BodyPosition\Helper;


class positionHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    private $_valuePosition = [
        '1' => ['x' => '10', 'y' => '50'],
        '2' => ['x' => '50', 'y' => '50'],
        '3' => ['x' => '10', 'y' => '10'],
        '4' => ['x' => '50', 'y' => '10'],
    ];


    public function validatePositionValue($positionValue)
    {
        if(in_array($positionValue, $this->getValuePosition())) {
            return true;
        }   else {
            return false;
        }

    }



    protected function getValuePosition()
    {
        return $this->_valuePosition;
    }
}