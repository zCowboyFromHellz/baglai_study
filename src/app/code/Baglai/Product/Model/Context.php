<?php


namespace Baglai\Product\Model;

use \Magento\Framework\File\Csv;


class Context
{
    /**
     * @var FixtureManager
     */
    protected $fixtureManager;

    /**
     * @var Csv
     */
    private $csvReader;

    /**
     * @param FixtureManager $fixtureManager
     * @param Csv $csvReader
     */
    public function __construct(FixtureManager $fixtureManager, Csv $csvReader)
    {
        $this->fixtureManager = $fixtureManager;
        $this->csvReader = $csvReader;
    }

    /**
     * @return FixtureManager
     */
    public function getFixtureManager()
    {
        return $this->fixtureManager;
    }

    /**
     * @return Csv
     */
    public function getCsvReader()
    {
        return $this->csvReader;
    }
}