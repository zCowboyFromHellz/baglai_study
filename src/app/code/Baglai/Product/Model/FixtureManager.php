<?php

namespace Baglai\Product\Model;

use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\Directory\ReadInterface;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Framework\View\Asset\Repository;

class FixtureManager
{
    /**
     * @var ComponentRegistrar
     */
    private $componentRegistrar;

    /**
     * Modules root directory
     *
     * @var ReadInterface
     */

    /**
     * @var StringUtils
     */
    protected $_string;

    /**
     * @param ComponentRegistrar $componentRegistrar
     * @param StringUtils $string
     */
    public function __construct(ComponentRegistrar $componentRegistrar, StringUtils $string)
    {
        $this->componentRegistrar = $componentRegistrar;
        $this->_string = $string;
    }

    /**
     * @param string $fileId
     * @return string
     * @throws LocalizedException
     */
    public function getFixture($fileId)
    {
        list($moduleName, $filePath) = Repository::extractModule(
            $this->normalizePath($fileId)
        );
        return $this->componentRegistrar->getPath(ComponentRegistrar::MODULE, $moduleName) . '/' . $filePath;
    }

    /**
     * Remove excessive "." and ".." parts from a path
     *
     * For example foo/bar/../file.ext -> foo/file.ext
     *
     * @param string $path
     * @return string
     */
    public static function normalizePath($path)
    {
        $parts = explode('/', $path);
        $result = [];

        foreach ($parts as $part) {
            if ('..' === $part) {
                if (!count($result) || ($result[count($result) - 1] == '..')) {
                    $result[] = $part;
                } else {
                    array_pop($result);
                }
            } elseif ('.' !== $part) {
                $result[] = $part;
            }
        }
        return implode('/', $result);
    }
}