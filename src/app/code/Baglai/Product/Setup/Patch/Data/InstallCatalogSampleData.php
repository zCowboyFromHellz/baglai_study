<?php

namespace Baglai\Product\Patch\Data;

use Baglai\Product\Setup\Installer;
use Magento\Framework\Setup;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;

/**
 * Class InstallCatalogSampleData
 * @package Magento\CatalogSampleData\Setup\Patch\Data
 */
class InstallCatalogSampleData implements DataPatchInterface, PatchVersionInterface
{
    /**
     * @var Baglai\Product\Setup\Patch\Baglai\Product\Setup\Executor
     */
    protected $executor;

    /**
     * @var Installer
     */
    protected $installer;

    /**
     * InstallCatalogSampleData constructor.
     * @param \Baglai\Product\Setup\Patch\Baglai\Product\Setup\Executor $executor
     * @param \Baglai\Product\Setup\Patch\Installer $installer
     */
    public function __construct(
        \Baglai\Product\Setup\Patch\Baglai\Product\Setup\Executor $executor,
        \Baglai\Product\Setup\Patch\Installer $installer
    ) {
        $this->executor = $executor;
        $this->installer = $installer;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->executor->exec($this->installer);
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.0.0';
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}
