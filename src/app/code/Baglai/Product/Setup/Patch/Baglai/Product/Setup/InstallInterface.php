<?php


namespace Baglai\Product\Setup\Patch\Baglai\Product\Setup;


interface InstallInterface
{
    /**
     * Install SampleData module
     *
     * @return void
     */
    public function install();
}