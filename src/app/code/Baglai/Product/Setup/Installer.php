<?php

namespace Baglai\Product\Setup\Patch;

use Baglai\Product\Model\Product;
use Exception;
use Magento\CatalogSampleData\Model\Attribute;
use Magento\CatalogSampleData\Model\Category;

class Installer implements Baglai\Product\Setup\InstallInterface
{
    /**
     * Setup class for category
     *
     * @var Category
     */
    protected $categorySetup;

    /**
     * Setup class for product attributes
     *
     * @var Attribute
     */
    protected $attributeSetup;

    /**
     * Setup class for products
     *
     * @var Product
     */
    protected $productSetup;

    /**
     * @param \Baglai\Product\Model\Category $categorySetup
     * @param \Baglai\Product\Model\Attribute $attributeSetup
     * @param Product $productSetup
     */
    public function __construct(
        \Baglai\Product\Model\Category $categorySetup,
        \Baglai\Product\Model\Attribute $attributeSetup,
        Product $productSetup
    ) {
        $this->categorySetup = $categorySetup;
        $this->attributeSetup = $attributeSetup;
        $this->productSetup = $productSetup;
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function install()
    {
        $this->attributeSetup->install(['Magento_CatalogSampleData::fixtures/attributes.csv']);
        $this->categorySetup->install(['Baglai_Product::fixtures/categories.csv']);
        $this->productSetup->install(
            [
                'Baglai_Product::fixtures/SimpleProduct/products.csv',
            ],
            [
//                'Magento_CatalogSampleData::fixtures/SimpleProduct/images_gear_bags.csv',
//                'Magento_CatalogSampleData::fixtures/SimpleProduct/images_gear_fitness_equipment.csv',
//                'Magento_CatalogSampleData::fixtures/SimpleProduct/images_gear_watches.csv',
            ]
        );
    }
}
