define([
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        rendererList.push(
            {
                type: 'baglai_pashcoin',
                component: 'Baglai_PayMethod/js/view/payment/renderer-list/pashcoin'
            }
        );

        /** Add view logic here if needed */
        return Component.extend({});
    });