define([
        'jquery',
        'Magento_Payment/js/view/payment/cc-form'
    ],
    function ($, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Baglai_PayMethod/payment/pashcoin'
            },

            context: function() {
                return this;
            },

            getCode: function() {
                return 'baglai_pashcoin';
            },

            isActive: function() {
                return true;
            }
        });
    }
);