<?php


namespace Baglai\Kicks\Model\Config\Backend;


class CustomFileType extends \Magento\Config\Model\Config\Backend\File
{
    protected $indexHelper;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
        \Magento\Config\Model\Config\Backend\File\RequestData\RequestDataInterface $requestData,
        \Magento\Framework\Filesystem $filesystem,
        \Baglai\Kicks\Import\Controller\Index\Index $indexHelper,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])
    {
        $this->indexHelper = $indexHelper;
        parent::__construct(
            $context,
            $registry,
            $config,
            $cacheTypeList,
            $uploaderFactory,
            $requestData,
            $filesystem,
            $resource,
            $resourceCollection,
            $data);
    }


    /**
     * @return string[]
     */
    public function getAllowedExtensions() {
        return ['csv'];
    }
    public function beforeSave()
    {
$this->indexHelper->execute();
    }
}